#include "Logger.hpp"
#include "EventHandler.hpp"
#include "KeyboardEvent.hpp"
#include "KeyboardController.hpp"
#include "Event.hpp"
#include <sstream>

void Logger::handleEvent(std::shared_ptr<Event> event){
	std::shared_ptr<KeyboardEvent> e = std::dynamic_pointer_cast<KeyboardEvent>(event);
	if (e == 0){
		return;
	}
	std::stringstream ss;
	ss << "Received KeyBoardEvent: ";
	if (e->data.state == RELEASED){
		ss << "(" << e->data.key << ") " << e->description << " released";
	}
	else{
		ss << "(" << e->data.key << ") " << e->description << " pressed";
	}
	logInfo(ss.str());
}
Logger::~Logger() {

}