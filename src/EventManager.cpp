#include "EventManager.hpp"
#include "Event.hpp"
#include "EventHandler.hpp"
#include "Logger.hpp"
#include <sstream>
#include "ConsoleKeyboardEventHandler.hpp"
#include <memory>

unsigned int EventManager::handleSpecificEvents()
{
	// Handle specific events
	unsigned int specEventsHandled = 0;
	for (auto it = specificEvents.begin(); it != specificEvents.end(); ++it)
	{
		unsigned int eventId = it->first;
		unsigned int handlerId = it->second;
		handlers[handlerId]->handleEvent(events[eventId]);
		specEventsHandled++;
	}
	specificEvents.clear();
	return specEventsHandled;
}

unsigned int EventManager::handleEventsByType()
{
	// Handle events by type
	unsigned int typeEventsHandled = 0;
	// For every type
	for (std::pair<EventType, std::vector<unsigned int>> type_eventIds : eventsByType){
		// For every event in that type
		for (unsigned int eventId: type_eventIds.second){
			// Notify all handlers for that type
			for (unsigned int handlerId : handlersForType[type_eventIds.first]){
				handlers[handlerId]->handleEvent(events[eventId]);
			}
		}

	}
	eventsByType.clear();
	return typeEventsHandled;
}

unsigned int EventManager::handleGenericEvents()
{
	int genEventsHandled = 0;
	for (std::pair<unsigned int, std::shared_ptr<EventHandler<Event>>> thePair: handlers){
		std::shared_ptr<EventHandler<Event>> handler = thePair.second;
		for (unsigned int eventId : genericEvents){
			handler->handleEvent(events[eventId]);
		}

	}
	return genEventsHandled;
}
void EventManager::handleEvents()
{
	unsigned int specificEventsHandled = handleSpecificEvents();
	unsigned int typedEventsHandled = handleEventsByType();
	unsigned int genericEventsHandled = handleGenericEvents();
	
	// Remove all events
	events.clear();
	//qualifiersForEventById.clear();
}
void EventManager::notifyAll(std::shared_ptr<Event>event)
{
	this->events[event->_event_id] = event;
	this->genericEvents.push_back(event->_event_id);
}
void EventManager::registerGenericHandler(std::shared_ptr<EventHandler<Event>> handler)
{
	this->handlers[handler->_event_handler_id] = handler;
}

void EventManager::registerHandlerByType(std::shared_ptr<EventHandler<Event>> handler, EventType type)
{
	this->handlers[handler->_event_handler_id] = std::shared_ptr<EventHandler<Event>>(handler);
	if (this->handlersForType.find(type) == this->handlersForType.end())
	{
		this->handlersForType[type] = std::vector<unsigned int>();
	}
	this->handlersForType[type].push_back(handler->_event_handler_id);
}

void EventManager::notifyByType(std::shared_ptr<Event>event, EventType type){
	this->events[event->_event_id] = event;
	if (this->eventsByType.find(type) == this->eventsByType.end())
	{
		this->eventsByType[type] = std::vector<unsigned int>();
	}
	this->eventsByType[type].push_back(event->_event_id);
}

void EventManager::notifySpecific(std::shared_ptr<Event>event, std::shared_ptr<EventHandler<Event>> handler)
{
	this->specificEvents[event->_event_id] = handler->_event_handler_id;
}

void EventManager::registerHandlerByQualifiers(std::shared_ptr<EventHandler<Event>> handler, std::initializer_list<EventQualifier>)
{

}