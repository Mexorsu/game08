#pragma warning( disable : 4005 )
#include "ShootController.hpp"
#include "KeyMapping.hpp"
#include "GameLoop.hpp"

static const float NO_MORE_SHOOTING_FOR_YOU_MISTER_PERIOD = 4;
void ShootController::updateObjectStateFromInput(float deltaTime){
	// get mouse position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	// and reset it for next frame
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);
	float time = GameLoop::frameTime;
	if (glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_shoot)) == GLFW_PRESS ){
		if (time > lastShot + NO_MORE_SHOOTING_FOR_YOU_MISTER_PERIOD){
			object->shootBall();
		}
	}
}

ShootController::ShootController(GLFWwindow *window){
	this->window = window;
}

void ShootController::attachObject(GameLoop *object){
	this->object = object;
}