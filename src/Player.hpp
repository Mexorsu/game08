#pragma once
#include "Moveable.hpp"
#include "Camera.hpp"
#include "TestGameObject.hpp"
#include "EventHandler.hpp"

class Player : public TestGameObject{ //also inherits EventHandler interface from TestGameObject
public:
	std::shared_ptr<Camera> cam;
	float move_force = 300;
	glm::vec3 offset = glm::vec3(0,0,0);
	Player(std::shared_ptr<Camera> _cam);

	//Override (TestGameObject)
	void update(float dT);
	// Eventhandler part
	void handleEvent(std::shared_ptr<Event> event) override;
private:
};