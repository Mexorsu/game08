#pragma once
#include "Event.hpp"

enum MoveDirection {
	FORWARD, BACKWARD, UP, DOWN, LEFT, RIGHT
};

class MoveEvent : public Event {
public:
	MoveDirection direction;
	float dT;
    MoveEvent(MoveDirection direction);
	//Override
	~MoveEvent();
};