#pragma once
#include "Controller.hpp"
#include "GameLoop.hpp"
#include <GLFW/glfw3.h>

class ShootController : public Controller {
public:
	ShootController(GLFWwindow *window);
	void attachObject(GameLoop *object);
	void updateObjectStateFromInput(float deltaTime);
private:
	GameLoop *object;
	GLFWwindow *window;
	float lastShot = GameLoop::frameTime;

};