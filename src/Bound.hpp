#pragma once
#include "Renderable.hpp"
#include <memory>

enum BoundType {
	BOX, SPHERE, COMPLEX
};
struct Plane {
	glm::vec3 point;
	glm::vec3 norm;
};
class Bound{
public:
	BoundType type;
	std::shared_ptr<Renderable> parent;
	virtual bool checkForCollision(std::shared_ptr<Bound> otherBound, BoundType type) = 0;
};