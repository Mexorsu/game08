#include "CollisionEvent.hpp"
#include "Logger.hpp"

CollisionEvent::CollisionEvent(std::shared_ptr<TestGameObject> t1, std::shared_ptr<TestGameObject> t2){
	this->t1 = t1;
	this->t2 = t2;
}
//Override
CollisionEvent::~CollisionEvent() {
	this->t1._Decref();
	this->t2._Decref();

	Event::~Event();
}