#pragma once
#include <map>
#include <vector>
#include <memory>

class Event;
template <class T> class EventHandler;
enum EventType;
enum EventQualifier;


class EventManager {
public:
	static std::unique_ptr<EventManager> GlobalEventManager;
	void handleEvents();

	void registerGenericHandler(std::shared_ptr<EventHandler<Event>> handler);
	void registerHandlerByType(std::shared_ptr<EventHandler<Event>> handler, EventType type);
	void registerHandlerByQualifiers(std::shared_ptr<EventHandler<Event>> handler, std::initializer_list<EventQualifier>);

	void notifySpecific(std::shared_ptr<Event> event, std::shared_ptr<EventHandler<Event>> handler);
	void notifyAll(std::shared_ptr<Event> event);
	void notifyByType(std::shared_ptr<Event> event, EventType type);
	void notifyWithQualifiers(std::shared_ptr<Event> event, std::initializer_list<EventQualifier>);
private:
	std::map<unsigned int, std::shared_ptr<EventHandler<Event>>> handlers;
	std::map<unsigned int, std::shared_ptr<Event>> events;
	std::vector<unsigned int> genericEvents;
	// For notifySpecific()
	std::map<unsigned int, unsigned int> specificEvents;
	// For notifyByType()
	std::map<EventType, std::vector<unsigned int>> handlersForType;
	std::map<EventType, std::vector<unsigned int>> eventsByType;
	// For notifyWithQualifiers()
	std::map<unsigned int, std::vector<EventQualifier>> qualifiersForEventById;
	std::map<unsigned int, std::vector<EventQualifier>> qualifiersForEventHandlerById;
	unsigned int handleSpecificEvents();
	unsigned int handleEventsByType();
	unsigned int handleEventsByQualifiers();
	unsigned int handleGenericEvents();
};