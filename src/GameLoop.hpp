#pragma once
#include "Renderer.hpp"
#include "KeyboardController.hpp"
#include <vector>
#include "TestGameObject.hpp"
#include <memory>

class Player;

class GameLoop
{
public:
	static float frameTime;
	static float deltaTime;
	static float GameLoop::fps;
	GameLoop();
	~GameLoop();
	void start();
	void run();
	void pause();
	void stop();
	void shootBall();
private:
	std::shared_ptr<Renderer> renderer;
	float lastTime;
	std::shared_ptr<Camera> mainCamera;
	float getDeltaTime();
	std::vector<std::shared_ptr<Controller>> controllers;
	std::vector<std::shared_ptr<TestGameObject>> gameObjects;
	float accumulator = 0.0;
	std::shared_ptr<Player> player;
	};