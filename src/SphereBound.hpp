#pragma once
#include "Bound.hpp"

class BoxBound;

class SphereBound : public Bound {
public:
	std::shared_ptr<Renderable> parent;
	SphereBound(std::shared_ptr<Renderable> parent);
	bool checkForCollision(std::shared_ptr<Bound> otherBound, BoundType type);
	bool checkForCollisionWithSphere(std::shared_ptr<SphereBound>otherSphere);
	bool checkForCollisionWithBox(std::shared_ptr<BoxBound> box);
	float getRadius();
};