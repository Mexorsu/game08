#pragma warning( disable : 4005 )
#include <GL/glew.h>
#include "GameLoop.hpp"
#include "KeyMapping.hpp"
//#include <Windows.h>
#include "EventManager.hpp"
#include "ConsoleKeyboardEventHandler.hpp"
#include "KeyboardEvent.hpp"
#include <memory>

std::unique_ptr<EventManager> initGlobalEventManager(){
	std::unique_ptr<EventManager> result = std::make_unique<EventManager>();
	return result;
}

std::unique_ptr<EventManager> EventManager::GlobalEventManager = initGlobalEventManager();

int main(){
	system("pause");
	std::unique_ptr<GameLoop> loop = std::make_unique<GameLoop>();
	loop->start();
	system("pause");
	loop.release();
	//EventManager::GlobalEventManager.release();
	return 0;
}


