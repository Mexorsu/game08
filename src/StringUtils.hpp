#include <string>
#include <glm.hpp>
#include <sstream>

std::string vec3ToString(glm::vec3 vector){
	std::stringstream ss;
	ss << "[" << vector.x << "," << vector.y << "," << vector.z << "]";
	return ss.str();
}