#pragma once
#include <GL/glew.h>
#include <vector>
#include <glm.hpp>

//------------------------------------------
// This static int is used to initialize 
// Renderable::id field uniquely
//------------------------------------------
static int lastRenderableIdGiven = 0;
//------------------------------------------

class Renderable {
public:
	//------------------------------------------
	// Resources initialization data:
	//------------------------------------------
	int id;								//unique id of Renderable instance							  
	char * textureName = "brick.jpg";	//texture file path relative to ${INSTALLPATH}/resources dir
	char * objFileName = "bunker.obj";	//obj file path relative to ${INSTALLPATH}/resources dir
	bool initialized = false;					//this bool teels us if this Renderable have been initialied
										//										  with resource data
	bool loaded = false;
	//------------------------------------------
	// Object data:
	//------------------------------------------
	glm::vec3 __renderable_position;					//position of the object relative to center of the scene
	glm::vec3 __renderable_rotation;					//rotation of the object
	GLfloat __renderable_scale = 1.0;					//scale of the object

	// i make getters and setters to this virtual, to be overriden 
	virtual glm::vec3 getPosition() {
		return __renderable_position;
	}
	virtual glm::vec3 getRotation() {
		return __renderable_rotation;
	}
	virtual GLfloat getScale() {
		return __renderable_scale;
	}
	void setPosition(glm::vec3 &pos) {
		this->__renderable_position = pos;
	}
	void setRotation(glm::vec3 &rot) {
		this->__renderable_rotation = rot;
	}
	void setScale(GLfloat sca) {
		this->__renderable_scale = sca;
	}
	//------------------------------------------
	// GL identifiers (render loop data):
	//------------------------------------------
	GLuint texture;						//ID of texture for the object
	GLuint vertexBuffer;				//ID of vertex buffer for the object
	GLuint uvBuffer;					//ID of UV buffer for the object
	//------------------------------------------
	// Getters, setters, destructors, etc..:
	//------------------------------------------
	~Renderable();						//destructor
	int getVertexCount();
	std::vector<glm::vec3> 
				getVertexBuffer();
	std::vector<glm::vec3> 
				getNormalBuffer();
	std::vector<glm::vec2> 
				getUvBuffer();
	//------------------------------------------
	// Dev method for printing render data:
	//------------------------------------------
	void printRenderData();
	//------------------------------------------
	// This method loads obj data (vertices, 
	// uvs, normals, faces) from .obj file
	// into this instance of Renderable
	// - sets bool initialized to true
	//------------------------------------------
	void loadObjData(std::string resourceName);
private:
	//------------------------------------------
	// Vertices, uvs and normals, as read from 
	// .obj file
	//------------------------------------------
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uv;
	std::vector<glm::vec3> normals;
	//------------------------------------------
	// Indices, mapped from "faces" lines of
	// .obj file input
	//------------------------------------------
	std::vector<unsigned int> vertexIndices;
	std::vector<unsigned int> uvIndices;
	std::vector<unsigned int> normalIndices;
	// Vertices, uvs and normals, indexed
	// with use of above indices
	//------------------------------------------
	std::vector<glm::vec3> indexedVertices;
	std::vector<glm::vec2> indexedUv;
	std::vector<glm::vec3> indexedNormals;
};
