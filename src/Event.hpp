#pragma once
#include<string>
#include<iostream>

enum EventType {
	UNDEF, INPUT_EVENT, PHYSICS_EVENT, AI_EVENT, COLLISION, PLAYER_EVENT, LOGGER_EVENT
};

enum EventQuialifier {
	PLAYER, PHYSICS
};

static unsigned int lastEventId = 0;

static unsigned int getNextEventId() {
	return ++lastEventId;
}

class Event {
public:
	unsigned int _event_id = getNextEventId();
	std::string description = "Event";
	virtual ~Event(){};
};