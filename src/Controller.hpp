#pragma once
class Controller {
public:
	virtual void updateObjectStateFromInput(float deltaTime) = 0;
};