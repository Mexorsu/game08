#define _CR1T_SECURE_NO_WARNINGS
#define STB_IMAGE_IMPLEMENTATION
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>
#include "Resources.hpp"
#include "Logger.hpp"
#include "STBImage.hpp"
#include <map>

static std::map<std::string, GLuint> pathToTextureID;

GLuint reallyLoadTexture(const char * imagepath){
	int x, y, n;
	unsigned char *data = stbi_load(getFullResourcePath(imagepath), &x, &y, &n, STBI_rgb);

	if (data == nullptr){
		std::stringstream ss;
		ss << "Failed to load texture " << imagepath;
		logError(ss.str());
	}
	std::stringstream ss;
	ss << "Loaded texture: width " << x << ",  height: " << y << ", n: " << n << ", data size: " << (x*y*n);
	doDebugToFile(ss.str(), _DEBUG_KEY_TEXTURE_LOAD);
	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glGenerateMipmap(GL_TEXTURE_2D);
	if (n == 3)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, x, y, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	else if (n == 4)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);

	return textureID;
}
GLuint loadTexture(const char * imagepath){
	if (pathToTextureID[imagepath] == NULL){
		pathToTextureID[imagepath] = reallyLoadTexture(imagepath);
	}
	return pathToTextureID[imagepath];
}