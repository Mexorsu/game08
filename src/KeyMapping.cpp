#include "KeyMapping.hpp"

int KeyMapping::getBaseGLFWKeyFor(ControllerInput name){
	return baseMapping[name];
}
std::string KeyMapping::toString(ControllerInput name){
	return humanReadable[name];
}

std::map<ControllerInput, int> defaulBaseMapping(){
	std::map<ControllerInput, int> defBaseMap;
	defBaseMap[__controller_forward] = GLFW_KEY_W;
	defBaseMap[__controller_backward] = GLFW_KEY_S;
	defBaseMap[__controller_right] = GLFW_KEY_D;
	defBaseMap[__controller_left] = GLFW_KEY_A;
	defBaseMap[__controller_up] = GLFW_KEY_SPACE;
	defBaseMap[__controller_down] = GLFW_KEY_LEFT_CONTROL;
	defBaseMap[__controller_impulseForward] = GLFW_KEY_I;
	defBaseMap[__controller_impulseBackward] = GLFW_KEY_K;
	defBaseMap[__controller_impulseLeft] = GLFW_KEY_J;
	defBaseMap[__controller_impulseRight] = GLFW_KEY_L;
	defBaseMap[__controller_impulseUp] = GLFW_KEY_O;
	defBaseMap[__controller_impulseDown] = GLFW_KEY_P;
	defBaseMap[__controller_gravity_switch] = GLFW_KEY_G;
	defBaseMap[__controller_bounce] = GLFW_KEY_B;
	defBaseMap[__controller_shoot] = GLFW_KEY_CAPS_LOCK;
	defBaseMap[__controller_scripting_mode] = GLFW_KEY_HOME;
	return defBaseMap;
}

std::map<ControllerInput, std::string> controllerInputToHumanReadable(){
	std::map<ControllerInput, std::string> humanReadableMap;
	humanReadableMap[__controller_forward] = "forward";
	humanReadableMap[__controller_backward] = "backward";
	humanReadableMap[__controller_right] = "right";
	humanReadableMap[__controller_left] = "left";
	humanReadableMap[__controller_up] = "up";
	humanReadableMap[__controller_down] = "down";
	humanReadableMap[__controller_impulseForward] = "impulseForward";
	humanReadableMap[__controller_impulseBackward] = "impulseBackward";
	humanReadableMap[__controller_impulseLeft] = "impulseLeft";
	humanReadableMap[__controller_impulseRight] = "impulseRight";
	humanReadableMap[__controller_impulseUp] = "impulseUp";
	humanReadableMap[__controller_impulseDown] = "impulseDown";
	humanReadableMap[__controller_gravity_switch] = "gravity_switch";
	humanReadableMap[__controller_bounce] = "bounce";
	humanReadableMap[__controller_shoot] = "shoot";
	humanReadableMap[__controller_scripting_mode] = "scripting_mode";
	return humanReadableMap;
}
std::map<ControllerInput, int> KeyMapping::baseMapping = defaulBaseMapping();
std::map<ControllerInput, std::string> KeyMapping::humanReadable = controllerInputToHumanReadable();
