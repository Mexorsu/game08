#pragma once
#include <GL/glew.h>
#include <glm.hpp>
#include "Camera.hpp"
#include "Renderable.hpp"
#include <vector>
#include <map>
#include "TestGameObject.hpp"
#include <memory>

class Renderer {
public:
	//------------------------------------------
	// Fields:
	//------------------------------------------
	GLFWwindow *window; //window this renderer is 
						//rendering to
	std::shared_ptr<Camera> camera;	//and camera used for 
						//rendering
	int newRenderables = 0;//how many new renderables
						//to be initialised?
	std::vector<std::shared_ptr<TestGameObject>> renderables;
	//------------------------------------------
	// Lifecycle methods:
	//------------------------------------------
	void init();
	void update(float deltaT);
	void cleanUp();
	//------------------------------------------
	//  Getters, setters, adders, etc:
	//------------------------------------------
	void setCamera(std::shared_ptr<Camera> camera);
	void addRenderable(std::shared_ptr<TestGameObject> renderable);
	std::shared_ptr<Renderable> getRenderable(int renderableID);
	void removeRenderable(int renderableID);
private:
	//------------------------------------------
	// GL IDs:
	//------------------------------------------
	GLuint programID;			//shader id
	GLuint MatrixID;			//MVP matrix id
	GLuint TextureID;			//texture buffer id
	GLuint VertexArrayID;		//vertex array id
	//------------------------------------------
	// Matrices:
	//------------------------------------------
	glm::mat4 ViewMatrix;		//view matrix
	glm::mat4 ProjectionMatrix; //projection matrix
	//------------------------------------------
	// Other:
	//------------------------------------------
	void Renderer::initNewRenderables();
	std::map<int, int> renderLogPeriodicKeys;
};