#include "ConsoleKeyboardEventHandler.hpp"
#include "KeyboardEvent.hpp"
#include "Logger.hpp"
#include <sstream>
#include <memory>

void ConsoleKeyboardEventHandler::handleEvent(std::shared_ptr<Event> event){
	std::shared_ptr<KeyboardEvent> e = std::dynamic_pointer_cast<KeyboardEvent>(event);
	if (e != 0)
	{
		std::stringstream ss;
		if (e->data.state == PRESSED)
		{
			ss << "Pressed " << e->data.key << " key";
		}
		else
		{
			ss << "Released " << e->data.key << " key";
		}
		logInfo(ss.str());
	}
	else
	{
		logError("Wrong event type!");
	}
}
ConsoleKeyboardEventHandler::~ConsoleKeyboardEventHandler() {
}