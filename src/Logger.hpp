#ifndef __LOGGER_SAFEGUARD
#define __LOGGER_SAFEGUARD
#include <GL/glew.h>
#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
#include<map>
#include <GLFW/glfw3.h>
#include <memory>
#include "EventHandler.hpp"

// put your debug keys here
enum DebugKeys {
	_DEBUG_KEY_EVENTS, _DEBUG_KEY_MEMORY, _DEBUG_KEY_OBJECT_LOAD, _DEBUG_KEY_TEXTURE_LOAD, _DEBUG_KEY_KEYBOARD_EVENTS_PRESSED, _DEBUG_KEY_KEYBOARD_EVENTS_RELEASED
};

//switch your debug keys on/off here
static std::map<int, bool> debugStateForKey
{ 
	{ _DEBUG_KEY_EVENTS, true },
	{ _DEBUG_KEY_MEMORY, true },
	{ _DEBUG_KEY_OBJECT_LOAD, false },
	{ _DEBUG_KEY_TEXTURE_LOAD, true },
	{ _DEBUG_KEY_KEYBOARD_EVENTS_PRESSED, false },
	{ _DEBUG_KEY_KEYBOARD_EVENTS_RELEASED, true }
};
static std::map<int, float> lastLogTimeForKey;
static std::map<int, float> periodForKey;
static std::ofstream log_file("C:\\DEV\\Projects\\Game08\\log.txt");
static bool logToFile = false;
static bool logToConsole = true;
static std::string sep = "------------------------------------------------------------------------";
static int lastLogPeriodicKey = 0;

	static void logInternal(std::string text);
	static void logCriticalError(std::string error){
		std::stringstream ss;
		ss << sep << std::endl << "CRITICAL ERROR: " << error << std::endl << sep << std::endl;
		logInternal(ss.str());
	}
	static void logError(std::string error){
		std::stringstream ss;
		ss << sep << std::endl << "ERROR: " << error << std::endl << sep << std::endl;
		logInternal(ss.str());
	}
	static void logWarning(std::string warning){
		std::stringstream ss;
		ss << sep << std::endl << "WARNING: " << warning << std::endl << sep << std::endl;
		logInternal(ss.str());
	}
	static void logInfo(std::string info){
		std::stringstream ss;
		ss << sep << std::endl << "INFO: " << info << std::endl << sep << std::endl;
		logInternal(ss.str());
	}

	static void doLogToFile(std::string theLog){
		if (!log_file){
			logError("Cannot open log file");
			return;
		}
		else{
			log_file << theLog;
		}
	}

	static void doDebugToFile(std::string theLog, int debugKey){
		if (debugStateForKey[debugKey] == false){
			return;
		}
		if (!log_file){
			logError("Cannot open log file");
			return;
		}
		else{
			log_file << theLog;
		}
	}
	static void logInternal(std::string text){
		if (logToFile){
			doLogToFile(text);
		}
		if (logToConsole){
			std::cout << text;
		}
	}
	static void debug(std::string info, int debugKey){
		if (debugStateForKey[debugKey] == false) return;
		std::stringstream ss;
		ss << sep << std::endl << "DEBUG: " << info << std::endl << sep << std::endl;
		logInternal(ss.str());
	}
	static void debugPeriodic(std::string info, int debugKey, int logPeriodicKey){
		if (debugStateForKey[debugKey] == false) return;
		if (lastLogTimeForKey[logPeriodicKey] + periodForKey[logPeriodicKey] < glfwGetTime()){
			std::stringstream ss;
			ss << sep << std::endl << "DEBUG: " << info << std::endl << sep << std::endl;
			logInternal(ss.str());
			lastLogTimeForKey[logPeriodicKey] = (float)glfwGetTime();
		}
	}
	static void debug(std::string info, std::initializer_list<int> debugKeys){
		for (int key : debugKeys){
			if (debugStateForKey[key] == false) return;
		}
		std::stringstream ss;
		ss << sep << std::endl << "DEBUG: " << info << std::endl << sep << std::endl;
		logInternal(ss.str());
	}
	static void debugPeriodic(std::string info, std::initializer_list<int> debugKeys, int logPeriodicKey){
		for (int key : debugKeys){
			if (debugStateForKey[key] == false) return;
		}
		if (lastLogTimeForKey[logPeriodicKey] + periodForKey[logPeriodicKey] < glfwGetTime()){
			std::stringstream ss;
			ss << sep << std::endl << "DEBUG: " << info << std::endl << sep << std::endl;
			logInternal(ss.str());
			lastLogTimeForKey[logPeriodicKey] = (float) glfwGetTime();
		}
	}
	static int getLogPeriodicKey(float period) {
		glfwGetTime();
		int newLogPeriodicKey = lastLogPeriodicKey + 1;
		lastLogPeriodicKey += 1;
		lastLogTimeForKey[newLogPeriodicKey] = (float)glfwGetTime();
		periodForKey[newLogPeriodicKey] = period;
		return newLogPeriodicKey;
	}
	static void logPeriodic(std::string text, int logPeriodicKey) {
		if (lastLogTimeForKey[logPeriodicKey] + periodForKey[logPeriodicKey] < glfwGetTime()){
			std::string sep = "------------------------------------------------------------------------";
			std::stringstream ss;
			ss << sep << std::endl << "INFO: " << text << std::endl << sep << std::endl;
			logInternal(ss.str());
			lastLogTimeForKey[logPeriodicKey] = (float) glfwGetTime();
		}
	}
	static void freeLogPeriodicKey(int key) {
		lastLogTimeForKey[key] = NULL;
		periodForKey[key] = NULL;
	}

	class Logger : public EventHandler<Event>{
	public:
		std::stringstream line = std::stringstream("");
		void handleEvent(std::shared_ptr<Event> event);
		~Logger();
	};
#endif