#pragma once
#include <glm.hpp>
class Movable {
public:
	virtual void moveForward(float step) = 0;
	virtual void moveBackwards(float step) = 0;
	virtual void moveLeft(float step) = 0;
	virtual void moveRight(float step) = 0;
	virtual void moveUpwards(float step) = 0;
	virtual void moveDownwards(float step) = 0;
};