#include "PhysicalObject.hpp"
#include "GameLoop.hpp"

static const float CLOSE_TO_ZERO = 0.001f;
static const glm::vec3 GRAVITY(0.0, -9.1, 0.0); // yeah far fetched but for now 'feels right' ;)

//#include "PhysicalObject.hpp"

float clampNearZeroToZero(float input){
	if (input < 0.001 && input > 0){
		return 0.0;
	}
	else if (input > -0.001 && input < 0){
		return 0.0;
	}
	else{
		return input;
	}
}

void PhysicalObject::applyImpulseForce(glm::vec3 force)
{
	this->force += force;
}
void PhysicalObject::applyImpulseForce(glm::vec3 force, float impulseTime)
{
	unsigned int impulseID = ++lastIpulseKey;
	impulseForce[impulseID] = force;
	impulseEnd[impulseID] = (float) GameLoop::frameTime + impulseTime;
}
// simple euler integration
void PhysicalObject::update(float dT, glm::vec3 &position, glm::vec3 &rotation)
{
	//if (applyGravity && acceleration.y > GRAVITY.y){
	//	velocity = velocity + GRAVITY *dT;
	//}
	float length = (float)velocity.length();
	glm::vec3 dampeningForce;
	if (onGround)
	{
		dampeningForce = -groundFriction*velocity;// = -airFriction*glm::normalize(velocity)*length;
	}
	else
	{
		dampeningForce = -airFriction*velocity;
	}
	force = force + dampeningForce;
	acceleration = (force / mass);
	velocity = velocity + (acceleration * (float)dT);
	position = position + (velocity * (float)dT);

	// reset the force
	force = glm::vec3(0.0, 0.0, 0.0);

	float time = GameLoop::frameTime;

	// re-apply long force impulses
	auto itr = impulseEnd.begin();
	while (itr != impulseEnd.end())
	{
		if (itr->second <= time)
		{
			itr = impulseEnd.erase(itr);
		}
		else
		{
			force += impulseForce[itr->first];
			itr++;
		}
	}

	//rotation.y += 0.01;
}