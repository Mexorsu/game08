#pragma warning( disable : 4005 )
#include "Player.hpp"
#include <memory>
#include "MoveEvent.hpp"
#include "EventManager.hpp"
#include "Logger.hpp"
#include "GameLoop.hpp"

Player::Player(std::shared_ptr<Camera> _cam) : TestGameObject(SPHERE) {
	this->cam = _cam;
	this->body->mass = 1;
}

void Player::update(float dT){
	TestGameObject::update(dT);
	glm::vec3 camPos = this->getPosition();
	camPos.y += offset.y;
	camPos += (glm::normalize(-cam->getDirection()*(float)4));
	this->cam->setPosition(camPos);
}

void Player::handleEvent(std::shared_ptr<Event> event){
	//TestGameObject::handleEvent(event);
	if (std::dynamic_pointer_cast<MoveEvent>(event)){
		std::shared_ptr<MoveEvent> e = std::dynamic_pointer_cast<MoveEvent>(event);
		switch (e->direction){
		case FORWARD:
			this->body->applyImpulseForce((cam->getDirection()* GameLoop::deltaTime * (float)move_force), .1f);
			break;
		case BACKWARD:
			this->body->applyImpulseForce(-(cam->getDirection() *  GameLoop::deltaTime * (float)move_force), .1f);
			break;
		case LEFT:
			this->body->applyImpulseForce(-(cam->right *  GameLoop::deltaTime * (float)move_force), .1f);
			break;
		case RIGHT:
			this->body->applyImpulseForce((cam->right *  GameLoop::deltaTime * (float)move_force), .1f);
			break;
		case UP:
			this->body->applyImpulseForce((cam->up *  GameLoop::deltaTime * (float)move_force), .1f);
			break;
		case DOWN:
			this->body->applyImpulseForce(-(cam->up *  GameLoop::deltaTime * (float)move_force), .1f);
			break;
		default:
			logError("Wrong direction MoveEvent!");
		}
	}
}
