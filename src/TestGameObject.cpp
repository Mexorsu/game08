#include "TestGameObject.hpp"
#include "Event.hpp"
#include "CollisionEvent.hpp"
#include "EventManager.hpp"
#include "Logger.hpp"

TestGameObject::TestGameObject(BoundType type){ //TODO: switch to switch right here
	this-> body = std::make_shared<PhysicalObject>();
	if (type == BOX){
		this->bound = std::make_shared<BoxBound>(std::shared_ptr<TestGameObject>(this));
	}
	else if (type == SPHERE){
		{
			this->bound = std::make_shared<SphereBound>(std::shared_ptr<TestGameObject>(this));
		}
	}
	EventManager::GlobalEventManager->registerHandlerByType(std::shared_ptr<EventHandler>(this),COLLISION);
}
void TestGameObject::update(float dT){
	body->update(dT, __renderable_position, __renderable_rotation);

}
bool TestGameObject::checkCollision(std::shared_ptr<TestGameObject>other){
	if (this->bound->checkForCollision(other->bound, other->bound->type)){
		return true;
		std::shared_ptr<CollisionEvent> event = std::make_shared<CollisionEvent>(std::shared_ptr<TestGameObject>(this), other);
		EventManager::GlobalEventManager->notifyByType(event, COLLISION);
	}
	return false;
	//return this->bound->checkForCollision(other->bound, other->bound->type);
}

float fToPositiveF(float abs){
	if (abs > 0.0)
		return abs;
	else
		return -abs;
}

void TestGameObject::handleEvent(std::shared_ptr<Event> event) {
	if (std::dynamic_pointer_cast<CollisionEvent>(event)){
		std::shared_ptr<CollisionEvent> e = std::dynamic_pointer_cast<CollisionEvent>(event);
		std::stringstream ss;
		ss << "Collision between: " << e->t1->id << " : [" << e->t1->getPosition().x << "," << e->t1->getPosition().y << "," << e->t1->getPosition().z << "]"
			<< " and " << e->t2->id << " : [" << e->t2->getPosition().x << ", " << e->t2->getPosition().y << ", " << e->t2->getPosition().z << "]";
		logInfo(ss.str());
	}
}

void TestGameObject::handleCollision(std::shared_ptr<TestGameObject>other){
	glm::vec3 tMomentum = this->body->velocity * this->body->mass;
	glm::vec3 oMomentum = other->body->velocity * other->body->mass;


	if (glm::dot(other->getPosition() - this->getPosition(), this->body->velocity - other->body->velocity) < 0){
		// objects are moving apart, screw the collision
		return;
	}

	if (this->bound->type == SPHERE && other->bound->type == SPHERE) {

		float distance = sqrt(
			pow(
			this->getPosition().x - other->getPosition().x
			, 2) +
			pow(
			this->getPosition().y - other->getPosition().y
			, 2) +
			pow(
			this->getPosition().z - other->getPosition().z
			, 2)
			);
		;
		float sumRadius = this->getScale() + other->getScale();
		glm::vec3 offset(other->getPosition() - this->getPosition());
		float origLen = glm::length(offset);
		offset = glm::normalize(offset);
		if (origLen - sumRadius > 0){
			offset = offset * (origLen - sumRadius);
		}
		else{
			offset = offset * -(origLen - sumRadius);
		}
		this->setPosition(this->getPosition() - offset);

		if (fToPositiveF(glm::length(this->body->velocity)) < VELOCITY_MIN_CLAMP && fToPositiveF(glm::length(other->body->velocity)) < VELOCITY_MIN_CLAMP){
			return;
		}

		glm::vec3 collision = this->getPosition() - other->getPosition();
		distance = (float)collision.length();

		if (distance == 0.0) {              // hack to avoid div by zero
			collision = glm::vec3(1.0, 0.0, 0.0);
			distance = 1.0;
		}
		collision = collision / distance;


		glm::vec3 x = collision;

		glm::vec3 v1 = this->body->velocity;
		glm::vec3 v2 = other->body->velocity;

		float x1 = glm::dot(this->body->velocity, collision);
		float x2 = glm::dot(other->body->velocity, collision);

		glm::vec3 v1x = x *x1;
		glm::vec3 v1y = v1 - v1x;
		float m1 = this->body->mass;

		glm::vec3 v2x = x *x2;
		glm::vec3 v2y = v2 - v2x;
		float m2 = other->body->mass;


		float spring = this->body->springFactor * other->body->springFactor;
		glm::vec3 s1vel = (v1x*(m1 - m2) / (m1 + m2) +
			v2x*(2 * m2) / (m1 + m2) +
			v1y);
		glm::vec3 s2vel = (v1x*(2 * m1) / (m1 + m2) +
			v2x*(m2 - m1) / (m1 + m2) +
			v2y);
		this->body->velocity = s1vel*spring;
		other->body->velocity = s2vel*spring;

		//if (m1 > m1){
		//	this->body->velocity = glm::vec3(0, 0, 0);
		//}
		//else{
		//	other->body->velocity = glm::vec3(0,0,0);
		//}

		/*if (this->body->mass == other->body->mass){
		float acf;
		if (bci != 0.0){
		acf = bci;
		}
		else{
		acf = -aci*0.1;
		}
		float bcf;
		if (aci != 0.0){
		bcf = aci;
		}
		else{
		bcf = -bci*0.1;
		}
		this->body->velocity += (acf - aci) * collision;
		other->body->velocity += (bcf - bci) * collision;
		}*/
	}
	else if (this->bound->type == SPHERE && other->bound->type == BOX) {
		this->body->velocity = glm::vec3(0, 0, 0);
	}
	else if (this->bound->type == BOX && other->bound->type == BOX) {
	}
	else if (this->bound->type == BOX && other->bound->type == BOX) {
	}


	glm::vec3 direction1;
	glm::vec3 springForce;
}
std::shared_ptr<PhysicalObject> TestGameObject::getBody(){
	return body;
}
void TestGameObject::applyImpulseForce(glm::vec3 force){
	body->applyImpulseForce(force);
}