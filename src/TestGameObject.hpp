#pragma once
#include "Renderable.hpp"
#include "PhysicalObject.hpp"
#include <glm.hpp>
#include "BoxBound.hpp"
#include "SphereBound.hpp"
#include "Bound.hpp"
#include <memory>
#include "EventHandler.hpp"
#include "Event.hpp"

static const float VELOCITY_MIN_CLAMP = 0.1f;
class TestGameObject : public Renderable, public EventHandler<Event> {
public:
	std::shared_ptr<PhysicalObject> body;
	std::shared_ptr<Bound> bound;

	TestGameObject(BoundType type);
	virtual void update(float dT);
	bool checkCollision(std::shared_ptr<TestGameObject> other);
	void handleCollision(std::shared_ptr<TestGameObject> other);
	std::shared_ptr<PhysicalObject> getBody();
	void applyImpulseForce(glm::vec3 force);
	virtual void handleEvent(std::shared_ptr<Event> event) override;
private:
};