#pragma once
#include<string>
#include<sstream>
static std::string path_separator = "\\";
static std::string dev_resources_dir = "C:\\DEV\\Projects\\Game08\\resources";
static char* getFullResourcePath(std::string resourceName){
	std::stringstream ss;
	ss << dev_resources_dir << path_separator << resourceName;
	std::string s = ss.str();
	char *a = new char[s.size() + 1];
	a[s.size()] = 0;
	memcpy(a, s.c_str(), s.size());
	return a;
}
