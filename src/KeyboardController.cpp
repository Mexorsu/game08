#include "KeyboardController.hpp"
#include "KeyMapping.hpp"
#include <iostream>
#include "Logger.hpp"
#include "EventManager.hpp"
#include "KeyboardEvent.hpp"
#include "MoveEvent.hpp"
#include <memory>
#include "GameLoop.hpp"
#include <GLFW/glfw3.h>
#include <functional>
#include <set>

bool KeyboardController::scriptingMode = false;
static bool wasLogToConsole = false;
static bool wasLogToFile = false;
static std::set<int> pressedKeys;
static std::set<int> issuedComands;
//from tutorial01 entry point
static GLFWCallBackObject glfwCallbackObject;

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	GLFWCallBackObject *sort_of_this = reinterpret_cast<GLFWCallBackObject*>(glfwGetWindowUserPointer(window));
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	std::string description;
	if (glfwKeyToString.find(key) != glfwKeyToString.end()){
		description = glfwKeyToString[key];
	}
	else{
		description = "UNKNOWN KEY!";
	}
	for (std::pair<int, std::shared_ptr<Event>> entry : sort_of_this->keyboardController->eventMapping) {
		if (glfwGetKey(window, entry.first) == GLFW_PRESS){
			EventManager::GlobalEventManager->notifyByType(entry.second, PLAYER_EVENT);
		}
	}
	if (action == GLFW_PRESS){
		if (debugStateForKey[_DEBUG_KEY_KEYBOARD_EVENTS_RELEASED] == true) {
			std::shared_ptr<KeyboardEvent> eventpress = std::make_shared<KeyboardEvent>(key, PRESSED);
			eventpress->description = description;
			EventManager::GlobalEventManager->notifyByType(eventpress, LOGGER_EVENT);
		}
		pressedKeys.emplace(key);
	}
	else if (action == GLFW_RELEASE && pressedKeys.find(key) != pressedKeys.end())
	{
		if (debugStateForKey[_DEBUG_KEY_KEYBOARD_EVENTS_RELEASED] == true) {
			std::shared_ptr<KeyboardEvent> eventrelease = std::make_shared<KeyboardEvent>(key, PRESSED);
			eventrelease->description = description;
			EventManager::GlobalEventManager->notifyByType(eventrelease, LOGGER_EVENT);
		}
	}
}

static void char_callback(GLFWwindow* window, unsigned int character) {
	//std::cout << static_cast<char>(character);
}

//extern "C" void charCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
//{
//	std::stringstream ss;
//	ss << "char: " << key << "state" << state;
//	std::cout << ss.str();
//}


KeyboardController::KeyboardController(GLFWwindow *window){
	this->window = window;
	glfwSetKeyCallback(window, &key_callback);
	glfwSetCharCallback(window, &char_callback);
	glfwCallbackObject.keyboardController = std::shared_ptr<KeyboardController>(this);
	glfwSetWindowUserPointer(window, &glfwCallbackObject);
	// Linking move keys to move events
	eventMapping[KeyMapping::getBaseGLFWKeyFor(__controller_forward)] = std::make_shared<MoveEvent>(FORWARD);
	eventMapping[KeyMapping::getBaseGLFWKeyFor(__controller_backward)] = std::make_shared<MoveEvent>(BACKWARD);
	eventMapping[KeyMapping::getBaseGLFWKeyFor(__controller_left)] = std::make_shared<MoveEvent>(LEFT);
	eventMapping[KeyMapping::getBaseGLFWKeyFor(__controller_right)] = std::make_shared<MoveEvent>(RIGHT);
	eventMapping[KeyMapping::getBaseGLFWKeyFor(__controller_up)] = std::make_shared<MoveEvent>(UP);
	eventMapping[KeyMapping::getBaseGLFWKeyFor(__controller_down)] = std::make_shared<MoveEvent>(DOWN);

}

KeyboardController::~KeyboardController(){
	glfwSetKeyCallback(this->window, NULL);
	glfwSetCharCallback(window, NULL);
}

void KeyboardController::attachObject(std::shared_ptr<EventHandler<Event>>object){
	this->object = object;
}

void KeyboardController::updateObjectStateFromInput(float deltaTime){
	//if (scriptingMode){
	//	handleScriptInput();
	//}
	//else
	//{
	//	handlePlayerMovement();
	//}
}

void KeyboardController::handlePlayerMovement() 
{
	/*if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS){
	std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('q',RELEASED);
	EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
	}
	*/
}
void KeyboardController::switchScriptingMode()
{
	if (scriptingMode == false){
		scriptingMode = true;
		if (logToFile){
			wasLogToFile = true;
		}
		if (logToConsole) {
			wasLogToConsole = true;
			logToConsole = false;
			logToFile = true;
		}

	}
	else
	{
		//reset state bools
		scriptingMode = false;
		if (wasLogToConsole) {
			logToConsole = true;
			wasLogToConsole = false;
		}
		if (wasLogToFile){
			logToFile = true;
			wasLogToFile = false;
		}
	}
}
void KeyboardController::handleScriptInput() 
{
		/*if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('q',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('w',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('e',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('r',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('t',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('y',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('u',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('i',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('o',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('p',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('a',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('s',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('d',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('f',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('g',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('h',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('j',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('k',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('l',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('z',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('x',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('c',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('v',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('b',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('n',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('m',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_ENTER) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('\n',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
		}
		if (glfwGetKey(window, GLFW_KEY_HOME) == GLFW_PRESS){
			std::shared_ptr<KeyboardEvent> event = std::make_shared<KeyboardEvent>('\n',RELEASED);
			EventManager::GlobalEventManager->notifyByType(event, INPUT_EVENT);
			switchScriptingMode();
			logInfo("Left Scripting mode");
		}*/

}