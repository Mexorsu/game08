#pragma warning( disable : 4005 )
#pragma once
#include "Bound.hpp"
#include "glm.hpp"
#include "Renderable.hpp"


class SphereBound;

class BoxBound : public Bound {
public:
	int logPeriodicKey;
	BoxBound(std::shared_ptr<Renderable> parent);
	glm::vec3 upNorm = glm::vec3(0, 1, 0);
	glm::vec3 downNorm = glm::vec3(0, -1, 0);
	glm::vec3 leftNorm = glm::vec3(-1, 0, 0);
	glm::vec3 rightNorm = glm::vec3(1, 0, 0);
	glm::vec3 frontNorm = glm::vec3(0, 0, 1);
	glm::vec3 backNorm = glm::vec3(0, 0, -1);

	float scale = 1;

	bool checkForCollision(std::shared_ptr<Bound> otherBound, BoundType type);
	bool checkForCollisionWithBox(std::shared_ptr<BoxBound> otherBox);
	bool checkForCollisionWithSphere(std::shared_ptr<SphereBound> sphere);
};