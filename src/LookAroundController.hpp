#pragma once
#include "Controller.hpp"
#include "ILookAround.hpp"
#include <GLFW/glfw3.h>
#include <memory>

class LookAroundController : public Controller {
public:
	LookAroundController(GLFWwindow *window);
	void attachObject(std::shared_ptr<ILookAround> object);
	void updateObjectStateFromInput(float deltaTime);
private:
	std::shared_ptr<ILookAround> object;
	GLFWwindow *window;
	
};