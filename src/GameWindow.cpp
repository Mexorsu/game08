#include "GameWindow.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm.hpp>

GameWindow::GameWindow()
{
	error = 0;
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		error = 1;
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(1024, 768, "Game08", NULL, NULL);
	if (window == NULL){
		fprintf(stderr, "Failed to open GLFW window, OpenGL version not supported\n");
		glfwTerminate();
		error = 2;
	}
	glfwMakeContextCurrent(window);
}


GameWindow::~GameWindow()
{
}
GLFWwindow* GameWindow::getWindow(){
	return this->window;
}
