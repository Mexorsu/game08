#pragma once
#include "Controller.hpp"
#include "TestGameObject.hpp"
#include <GLFW/glfw3.h>
#include "KeyMapping.hpp"
#include "glm.hpp"
#include "Logger.hpp"
#include <memory>
class TestController : public Controller {
private:
	std::shared_ptr<TestGameObject> object;
	GLFWwindow *window;
	std::map<std::string, float> keyCooldown;
	//KeyMapping *mapping;
public:
	TestController(GLFWwindow *window);
	void attachObject(std::shared_ptr<TestGameObject> object);
	void updateObjectStateFromInput(float deltaTime);
};