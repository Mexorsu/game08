#pragma warning( disable : 4005 )
#include "MoveEvent.hpp"
#include <string>
#include <sstream>
#include "GameLoop.hpp"
#include "Logger.hpp"

std::string getDescription(MoveEvent* self){
	std::stringstream ss;
	ss << "MoveEvent(";
	switch (self->direction){
	case UP:
		ss << "UP";
		break;
	case DOWN:
		ss << "DOWN";
		break;
	case LEFT:
		ss << "LEFT";
		break;
	case RIGHT:
		ss << "RIGHT";
		break;
	case FORWARD:
		ss << "FORWARD";
		break;
	case BACKWARD:
		ss << "BACKWARD";
		break;
	default:
		ss << "UNDEFINED_DIRECTION";
	}
	ss << ", " << self->dT << ")";
	return ss.str();
}

static int moveEventDestructionLogPeriodicKey = getLogPeriodicKey(1.0);
static int moveEventConstructorLogPeriodicKey = getLogPeriodicKey(1.0);

MoveEvent::MoveEvent(MoveDirection direction){
	this->direction = direction;
	this->dT = GameLoop::deltaTime;
	this->description = getDescription(this);
	//std::stringstream ss;
	//ss << "Move event created: " << this->toString();
	//debugPeriodic(ss.str() , { _DEBUG_KEY_EVENTS, _DEBUG_KEY_MEMORY }, moveEventConstructorLogPeriodicKey);
}
MoveEvent::~MoveEvent(){
	//debugPeriodic("Move event deallocated", { _DEBUG_KEY_EVENTS, _DEBUG_KEY_MEMORY }, moveEventDestructionLogPeriodicKey);
}