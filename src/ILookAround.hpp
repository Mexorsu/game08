#pragma once
class ILookAround {
public: 
	virtual void updateLookAtPoint(float step, float xpos, float ypos) = 0;
};