#include "LookAroundController.hpp"

void LookAroundController::updateObjectStateFromInput(float deltaTime){
	// get mouse position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	// and reset it for next frame
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);
	object->updateLookAtPoint(deltaTime, (float)xpos, (float)ypos);
}

LookAroundController::LookAroundController(GLFWwindow *window){
	this->window = window;
}

void LookAroundController::attachObject(std::shared_ptr<ILookAround> object){
	this->object = object;
}