#pragma once
#include "GL/glew.h"
#include "Event.hpp"
#include <memory>
#include "TestGameObject.hpp"
class CollisionEvent : public Event {
public:
	std::shared_ptr<TestGameObject> t1;
	std::shared_ptr<TestGameObject> t2;
	CollisionEvent(std::shared_ptr<TestGameObject> t1, std::shared_ptr<TestGameObject> t2);
	//Override
	~CollisionEvent();
};