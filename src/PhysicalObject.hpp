#pragma once
#include <gl/glew.h>
#include <windows.h>
#include<glm.hpp>
#include <sstream>
#include <map>
#include <GLFW\glfw3.h>

class PhysicalObject {
public:
	glm::vec3 velocity;
	glm::vec3 acceleration;
	glm::vec3 momentum;
	float springFactor = 0.2f;
	float airFriction = 0.1f;
	float groundFriction = 0.9f;
	glm::vec3 force = glm::vec3(0,0,0);
	GLfloat mass = 1;
	bool applyGravity = true;
	bool onGround = true;
	unsigned int lastIpulseKey = 0;
	std::map<unsigned int, float> impulseEnd;
	std::map<unsigned int, glm::vec3> impulseForce;

	void applyImpulseForce(glm::vec3 force);
	void applyImpulseForce(glm::vec3 force, float impulseTime);
	// simple euler integration
	void update(float dT, glm::vec3 &position, glm::vec3 &rotation);
	
};