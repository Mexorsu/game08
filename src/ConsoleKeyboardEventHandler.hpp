#pragma once
#include "EventHandler.hpp"

class ConsoleKeyboardEventHandler : public EventHandler<Event> {
public:
	void handleEvent(std::shared_ptr<Event> event);
	~ConsoleKeyboardEventHandler();
};