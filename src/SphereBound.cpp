#include "SphereBound.hpp"
#include "Bound.hpp"
#include "Logger.hpp"
#include "BoxBound.hpp"
#include <cmath> 


SphereBound::SphereBound(std::shared_ptr<Renderable>parent){
		this->type = SPHERE;
		this->parent = parent;
	}
bool SphereBound::checkForCollision(std::shared_ptr<Bound> otherBound, BoundType type){
		switch (type) {
		case BOX:
			//logInfo("Sphere-Box collisions not implemented");
			break;
		case SPHERE:
		{
			return checkForCollisionWithSphere(std::dynamic_pointer_cast<SphereBound>(otherBound));
			break;
		}
		case COMPLEX:
			//logInfo("Sphere-Complex collisions not implemented");
			break;
		default:
			//logError("Check for collision on wrong type bound");
			return false;
		}
		return false;
	}
float SphereBound::getRadius(){
	return this->parent->getScale();
}

bool SphereBound::checkForCollisionWithSphere(std::shared_ptr<SphereBound> otherSphere){
		float distance = sqrt(
			pow(
			this->parent->getPosition().x - otherSphere->parent->getPosition().x
			, 2) +
			pow(
			this->parent->getPosition().y - otherSphere->parent->getPosition().y
			, 2) +
			pow(
			this->parent->getPosition().z - otherSphere->parent->getPosition().z
			, 2)
			);


		float sumRadius = this->getRadius() + otherSphere->getRadius();
		return  distance < sumRadius;
	}
bool SphereBound::checkForCollisionWithBox(std::shared_ptr<BoxBound> box) {
		glm::vec3 center = this->parent->getPosition();

		Plane upperWall;
		upperWall.norm = box->upNorm;
		upperWall.point = box->parent->getPosition() + (box->upNorm*box->scale);
		glm::vec3 pointToCenter = upperWall.point - center;
		GLfloat distance = glm::dot(glm::normalize(upperWall.norm), pointToCenter);
		if (distance < -this->getRadius())
		{
			return false;
		}

		Plane lowerWall;
		lowerWall.norm = box->downNorm;
		lowerWall.point = box->parent->getPosition() + (box->downNorm*box->scale);

		pointToCenter = lowerWall.point - center;
		distance = glm::dot(glm::normalize(lowerWall.norm), pointToCenter);
		if (distance < -this->getRadius())
		{
			return false;
		}
		Plane rightWall;
		rightWall.norm = box->rightNorm;
		rightWall.point = box->parent->getPosition() + (box->rightNorm*box->scale);
		pointToCenter = rightWall.point - center;
		distance = glm::dot(glm::normalize(rightWall.norm), pointToCenter);
		if (distance < -this->getRadius())
		{
			return false;
		}

		Plane leftWall;
		leftWall.norm = box->leftNorm;
		leftWall.point = box->parent->getPosition() + (box->leftNorm*box->scale);
		pointToCenter = leftWall.point - center;
		distance = glm::dot(glm::normalize(leftWall.norm), pointToCenter);
		if (distance < -this->getRadius())
		{
			return false;
		}

		Plane frontWall;
		frontWall.norm = box->frontNorm;
		frontWall.point = box->parent->getPosition() + (box->frontNorm*box->scale);
		pointToCenter = frontWall.point - center;
		distance = glm::dot(glm::normalize(frontWall.norm), pointToCenter);
		if (distance < -this->getRadius())
		{
			return false;
		}

		Plane backWall;
		backWall.norm = box->backNorm;
		backWall.point = box->parent->getPosition() + (box->backNorm*box->scale);
		pointToCenter = backWall.point - center;
		distance = glm::dot(glm::normalize(backWall.norm), pointToCenter);
		if (distance < -this->getRadius())
		{
			return false;
		}
		return true;
	}
