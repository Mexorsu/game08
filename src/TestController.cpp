#include "TestController.hpp"
#include "GameLoop.hpp" // TODO: check out which files only include GameLoop for GameLoop::frameTime, replace by lightweight outer static header
#include "EventManager.hpp"

TestController::TestController(GLFWwindow *window){
	this->window = window;
	keyCooldown["gravity_switch"] = 0;
	keyCooldown["bounce"] = 0;
}
void TestController::attachObject(std::shared_ptr<TestGameObject> object){
	this->object = object;
}
void TestController::updateObjectStateFromInput(float deltaTime){
	if (glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_impulseForward)) == GLFW_PRESS){
		object->body->applyImpulseForce(glm::vec3(10.0, 0.0, 0.0));
	}
	if (glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_impulseBackward)) == GLFW_PRESS){
		object->body->applyImpulseForce(glm::vec3(-10.0, 0.0, 0.0));
	}
	if (glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_impulseLeft)) == GLFW_PRESS){
		object->body->applyImpulseForce(glm::vec3(0.0, 0.0, 10.0));
	}
	if (glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_impulseRight)) == GLFW_PRESS){
		object->body->applyImpulseForce(glm::vec3(0.0, 0.0, -10.0));
	}
	if (glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_impulseUp)) == GLFW_PRESS){
		object->body->applyImpulseForce(glm::vec3(0.0, 10.0, 0.0));
	}
	if (glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_impulseDown)) == GLFW_PRESS){
		object->body->applyImpulseForce(glm::vec3(0.0, -10.0, 0.0));
	}
	if ((glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_gravity_switch)) == GLFW_PRESS)
		&& keyCooldown["gravity_switch"]<GameLoop::frameTime){
		object->body->onGround = !object->body->onGround;
		keyCooldown["gravity_switch"] = GameLoop::frameTime + 1;
	}
	if ((glfwGetKey(window, KeyMapping::getBaseGLFWKeyFor(__controller_bounce)) == GLFW_PRESS)
		&& keyCooldown["bounce"]<GameLoop::frameTime){
		object->body->applyImpulseForce(glm::vec3(0.0f, 200.0f, 0.0f), 0.12f);
		object->body->onGround = false;
		keyCooldown["bounce"] = GameLoop::frameTime + 1;
	}
}