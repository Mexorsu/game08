#pragma warning( disable : 4005 )
#include "Renderer.hpp"
#include "Renderable.hpp"
#include "StringUtils.hpp"
#include "Logger.hpp"

void Renderer::setCamera(std::shared_ptr<Camera> camera){
	this->camera = camera;
}

void Renderer::addRenderable(std::shared_ptr<TestGameObject> renderable){
	this->renderables.push_back(renderable);
	newRenderables ++;
}
std::shared_ptr<Renderable> Renderer::getRenderable(int renderableID){
	for (std::shared_ptr<Renderable> renderable : renderables){
		if (renderable->id == renderableID){
			return renderable;
		}
	}
	return NULL;
}
void Renderer::removeRenderable(int renderableID){
	int position = -1;
	for (unsigned int i = 0; i < renderables.size(); i++){
		if (renderables[i]->id == renderableID){
			position = i;
		}
	}
	if (position != -1){
		renderables.erase(renderables.begin()+position);
	}
}

void Renderer::init(){
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(1024, 768, "Game08", NULL, NULL);
	if (window == NULL){
		fprintf(stderr, "Failed to open GLFW window, OpenGL version not supported\n");
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);

	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
	}
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("TransformVertexShader.vertexshader", "TextureFragmentShader.fragmentshader");

	// Shader data placeholders
	MatrixID = glGetUniformLocation(programID, "MVP");
	TextureID = glGetUniformLocation(programID, "myTextureSampler");
	this->camera = std::make_shared<Camera>(window);
}

void Renderer::initNewRenderables(){
	while (newRenderables>0){
		for (std::shared_ptr<Renderable> renderable : renderables) {
			if (!renderable->loaded){
				std::vector<glm::vec3> vertices = renderable->getVertexBuffer();
				std::vector<glm::vec2> uvs = renderable->getUvBuffer();
				GLuint vertexbuffer;
				glGenBuffers(1, &vertexbuffer);
				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
				glBufferData(GL_ARRAY_BUFFER, (vertices.size() * sizeof(glm::vec3)), &vertices[0], GL_STATIC_DRAW);

				GLuint uvbuffer;
				glGenBuffers(1, &uvbuffer);
				glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
				glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
				GLuint texID = loadTexture(renderable->textureName);
				renderable->texture = texID;
				renderable->vertexBuffer = vertexbuffer;
				renderable->uvBuffer = uvbuffer;
				renderable->initialized = true;
				renderLogPeriodicKeys[renderable->id] = getLogPeriodicKey(2.0);
				newRenderables--;
			}
		}
	}
}

void Renderer::update(float deltaTime) {
	initNewRenderables();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(programID);
	glActiveTexture(GL_TEXTURE0);

	this->camera->applyCameraToMatrices(deltaTime, &ViewMatrix, &ProjectionMatrix);

	for (std::shared_ptr<TestGameObject> renderable : renderables) {
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		// Translate
		ModelMatrix = glm::translate(ModelMatrix, renderable->getPosition());
		// Rotate
		ModelMatrix = glm::rotate(ModelMatrix, renderable->getRotation().x, glm::vec3(1, 0, 0));
		ModelMatrix = glm::rotate(ModelMatrix, renderable->getRotation().y, glm::vec3(0, 1, 0));
		ModelMatrix = glm::rotate(ModelMatrix, renderable->getRotation().z, glm::vec3(0, 0, 1));
		//Scale
		ModelMatrix = glm::scale(ModelMatrix, glm::vec3(renderable->getScale(), renderable->getScale(), renderable->getScale()));

		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		std::stringstream ss;
		ss << "Rendering " << renderable->id << " : " << renderable->getVertexCount();
		ss << " at position   :" << vec3ToString(renderable->getPosition()) << std::endl;;
		ss << " with rotation :" << vec3ToString(renderable->getRotation()) << std::endl;
		ss << " with velocity :" << vec3ToString(renderable->body->velocity) << std::endl;;
		ss << " and scale     :" << renderable->getScale();
		ss << std::endl;
		ss << "Vertex count   : " << renderable->getVertexCount() << std::endl;
		ss << "Vertex bufferID: " << renderable->vertexBuffer << std::endl;
		ss << "UV bufferID    : " << renderable->uvBuffer << std::endl;
		ss << "Texture ID     : " << renderable->texture << std::endl;
		//logPeriodic(ss.str(), renderLogPeriodicKeys[renderable.id]);


		glBindTexture(GL_TEXTURE_2D, renderable->texture);
		glUniform1i(TextureID, 0);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, renderable->vertexBuffer);
		glVertexAttribPointer(
			0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, renderable->uvBuffer);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			2,                                // size : U+V => 2
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);
		GLuint indices = renderable->getVertexCount();
		glDrawArrays(GL_TRIANGLES, 0, indices);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
	}
	// Swap buffers and poll events
	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Renderer::cleanUp(){
	// Cleanup shader, texture_bufer and VBO
	glDeleteProgram(programID);
	glDeleteTextures(1, &TextureID);
	glDeleteVertexArrays(1, &VertexArrayID);
	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}