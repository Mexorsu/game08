#pragma once
#include <map>
#include <iostream>
#include <map>
#include <GLFW/glfw3.h>
#include <string>

enum ControllerInput {
	__controller_forward,
	__controller_backward,
	__controller_right,
	__controller_left,
	__controller_up,
	__controller_down,
	__controller_impulseForward,
	__controller_impulseBackward,
	__controller_impulseLeft,
	__controller_impulseRight,
	__controller_impulseUp,
	__controller_impulseDown,
	__controller_gravity_switch,
	__controller_bounce,
	__controller_shoot,
	__controller_scripting_mode
};

class KeyMapping {
public:
	static int getBaseGLFWKeyFor(ControllerInput name);
	static std::string toString(ControllerInput name);
private:
	KeyMapping(){};
	static std::map<ControllerInput, int> baseMapping;
	static std::map<ControllerInput, std::string> humanReadable;
};
