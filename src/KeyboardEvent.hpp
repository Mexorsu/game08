#pragma once
#include "Event.hpp"

enum KeyState {
	PRESSED, RELEASED
};

struct KeyboardEventData {
	int key;
	KeyState state;
};

class KeyboardEvent : public Event {
public: 
	KeyboardEventData data;
	KeyboardEvent(int key, KeyState state);
	//Override
	void* getEventData();
	~KeyboardEvent();
};