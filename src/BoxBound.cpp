#include "BoxBound.hpp"
#include "Bound.hpp"
#include "Logger.hpp"
#include "SphereBound.hpp"
#include <sstream>
#include "glm.hpp"
#include "Renderable.hpp"
#include "GameLoop.hpp"

BoxBound::BoxBound(std::shared_ptr<Renderable> parent){
		this->parent = std::move(parent);
		//this->scale = parent->getScale();
		this->type = BOX;
		this->logPeriodicKey = getLogPeriodicKey(3.0);
	}
bool BoxBound::checkForCollision(std::shared_ptr<Bound> otherBound, BoundType type){
		switch (type) {
		case BOX:
		{
				checkForCollisionWithBox(std::dynamic_pointer_cast<BoxBound>(otherBound));
				break;
		}
		case SPHERE:
		{		 
				return checkForCollisionWithSphere(std::dynamic_pointer_cast<SphereBound>(otherBound));
				break;
		}
		case COMPLEX:
			//logInfo("Box-Complex collisions not implemented");
			break;
		default: 
			//logError("Check for collision on wrong type bound");
			return false;
		}
		return false;
	}
bool BoxBound::checkForCollisionWithBox(std::shared_ptr<BoxBound> otherBox){
		bool collideY = false;
		float oyUp = otherBox->parent->getPosition().y + otherBox->scale*upNorm.y;;
		float oyDown = otherBox->parent->getPosition().y + otherBox->scale*downNorm.y;

		float tyUp = this->parent->getPosition().y + this->scale*upNorm.y;
		float tyDown = this->parent->getPosition().y + this->scale*downNorm.y;

		if (oyUp >= tyDown && oyUp <= tyUp) collideY = true;
		if (oyUp >= tyUp && oyDown <= tyDown) collideY = true;
		if (oyDown >= tyDown && oyDown <= tyUp) collideY = true;

		bool collideX = false;
		float oxRight = otherBox->parent->getPosition().x + otherBox->scale*rightNorm.x;
		float oxLeft = otherBox->parent->getPosition().x + otherBox->scale*leftNorm.x;

		float txRight = this->parent->getPosition().x + this->scale*rightNorm.x;
		float txLeft = this->parent->getPosition().x + this->scale*leftNorm.x;

		if (oxRight >= txLeft && oxRight <= txRight) collideX = true;
		if (oxRight >= txRight && oxLeft <= txLeft) collideX = true;
		if (oxLeft >= txLeft && oxLeft <= txRight) collideX = true;

		bool collideZ = false;
		float ozFront = otherBox->parent->getPosition().z + otherBox->scale*frontNorm.z;
		float ozBack = otherBox->parent->getPosition().z + otherBox->scale*backNorm.z;

		float tzFront = this->parent->getPosition().z + this->scale*rightNorm.z;
		float tzBack = this->parent->getPosition().z + this->scale*leftNorm.z;

		if (ozFront >= tzBack && ozFront <= tzFront) collideZ = true;
		if (ozFront >= tzFront && ozBack <= tzBack) collideZ = true;
		if (ozBack >= tzBack && ozBack <= tzFront) collideZ = true;

		return collideX && collideY && collideZ;
	}
bool BoxBound::checkForCollisionWithSphere(std::shared_ptr<SphereBound> sphere){
		glm::vec3 center = sphere->parent->getPosition();

		Plane upperWall;
		upperWall.norm = upNorm;
		upperWall.point = parent->getPosition() + (upNorm*scale);
		glm::vec3 pointToCenter = upperWall.point - center;
		GLfloat distance = glm::dot(glm::normalize(upperWall.norm), pointToCenter);
		if (distance < -sphere->getRadius())
		{
			return false;
		}
		
		Plane lowerWall;
		lowerWall.norm = downNorm;
		lowerWall.point = parent->getPosition() + (downNorm*scale);

		pointToCenter = lowerWall.point - center;
		distance = glm::dot(glm::normalize(lowerWall.norm), pointToCenter);
		if (distance < -sphere->getRadius())
		{
			return false;
		}
		Plane rightWall;
		rightWall.norm = rightNorm;
		rightWall.point = parent->getPosition() + (rightNorm*scale);
		pointToCenter = rightWall.point - center;
		distance = glm::dot(glm::normalize(rightWall.norm), pointToCenter);
		if (distance < -sphere->getRadius())
		{
			return false;
		}

		Plane leftWall;
		leftWall.norm = leftNorm;
		leftWall.point = parent->getPosition() + (leftNorm*scale);
		pointToCenter = leftWall.point - center;
		distance = glm::dot(glm::normalize(leftWall.norm), pointToCenter);
		if (distance < -sphere->getRadius())
		{
			return false;
		}

		Plane frontWall;
		frontWall.norm = frontNorm;
		frontWall.point = parent->getPosition() + (frontNorm*scale);
		pointToCenter = frontWall.point - center;
		distance = glm::dot(glm::normalize(frontWall.norm), pointToCenter);
		if (distance < -sphere->getRadius())
		{
			return false;
		}

		Plane backWall;
		backWall.norm = backNorm;
		backWall.point = parent->getPosition() + (backNorm*scale);
		pointToCenter = backWall.point - center;
		distance = glm::dot(glm::normalize(backWall.norm), pointToCenter);
		if (distance < -sphere->getRadius())
		{
			return false;
		}

		std::stringstream ss;
		ss << "Col:  " << GameLoop::frameTime;
		logPeriodic(ss.str(), lastLogPeriodicKey);
		return true;
	}