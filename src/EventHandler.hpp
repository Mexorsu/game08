#pragma once
#include "Event.hpp"
#include <memory>

static unsigned int lastEventHandlerId = 0;

static unsigned int getNextEventHandlerId() {
	return ++lastEventHandlerId;
}

template <class T> class EventHandler {
public:
	virtual void handleEvent(std::shared_ptr<T> event) = 0;
	unsigned int _event_handler_id = getNextEventHandlerId();
	void noGenericsRestricionSupportInLanguageWorkaround(){
		// This won't compile if you were open minded enough 
		// to make an EventHandler handle something that's 
		// not even an event. Duuuh.
		Event *retardedPatheticWorkaround = new T();
	}
};